﻿using StoreWebsite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StoreWebsite.Controllers
{
    [RequireHttps]
    public class HomeController : Controller
    {

        private StoreDbContext db = new StoreDbContext();

        public ActionResult Index()
        {

            var item = db.Item;
            return View(item.ToList());
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


        [Authorize()]
        public ActionResult AuthenticatedOnly()
        {
            return Content("You are logged in!");
        }

        [Authorize(Roles = "Admin")]
        public ActionResult AdminOnly()
        {
            return Content("You are an admin!");
        }

    }
}