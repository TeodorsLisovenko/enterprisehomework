﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;
using StoreWebsite.Models;

namespace StoreWebsite.Controllers
{
    public class ShopItemController : Controller
    {
        private StoreDbContext db = new StoreDbContext();

        // GET: ShopItem
        [Authorize(Roles = "Shopkeeper, Admin")]
        public ActionResult Index()
        {
            var item = db.Item.Include(s => s.Category).Include(s => s.Shopkeeper);
            return View(item.ToList());
        }

        // GET: ShopItem/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ShopItemModel shopItemModel = db.Item.Find(id);
            if (shopItemModel == null)
            {
                return HttpNotFound();
            }
            return View(shopItemModel);
        }

        // GET: ShopItem/Create
        [Authorize(Roles = "Shopkeeper, Admin")]
        public ActionResult Create()
        {
            ViewBag.CategoryID = new SelectList(db.Category, "CategoryID", "CategoryName");
            ViewBag.ShopkeeperID = new SelectList(db.User, "ShopkeeperID", "Name");
            return View();
        }

        // POST: ShopItem/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "Shopkeeper, Admin")]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Reference,ItemName,Description,ImageURL,Price,ShopkeeperID,CategoryID")] ShopItemModel shopItemModel)
        {
            if (ModelState.IsValid)
            {
                db.Item.Add(shopItemModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CategoryID = new SelectList(db.Category, "CategoryID", "CategoryName", shopItemModel.CategoryID);
            ViewBag.ShopkeeperID = new SelectList(db.User, "ShopkeeperID", "Name", shopItemModel.ShopkeeperID);
            return View(shopItemModel);
        }

        // GET: ShopItem/Edit/5
        [Authorize(Roles = "Shopkeeper, Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ShopItemModel shopItemModel = db.Item.Find(id);
            if (shopItemModel == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryID = new SelectList(db.Category, "CategoryID", "CategoryName", shopItemModel.CategoryID);
            ViewBag.ShopkeeperID = new SelectList(db.User, "ShopkeeperID", "Name", shopItemModel.ShopkeeperID);
            return View(shopItemModel);
        }

        // POST: ShopItem/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "Shopkeeper, Admin")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Reference,ItemName,Description,ImageURL,Price,ShopkeeperID,CategoryID")] ShopItemModel shopItemModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(shopItemModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoryID = new SelectList(db.Category, "CategoryID", "CategoryName", shopItemModel.CategoryID);
            ViewBag.ShopkeeperID = new SelectList(db.User, "ShopkeeperID", "Name", shopItemModel.ShopkeeperID);
            return View(shopItemModel);
        }

        // GET: ShopItem/Delete/5
        [Authorize(Roles = "Shopkeeper, Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ShopItemModel shopItemModel = db.Item.Find(id);
            if (shopItemModel == null)
            {
                return HttpNotFound();
            }
            return View(shopItemModel);
        }

        // POST: ShopItem/Delete/5
        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "Shopkeeper, Admin")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ShopItemModel shopItemModel = db.Item.Find(id);
            db.Item.Remove(shopItemModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        //ItemDetails
        private ShopItemModel ItemDetail(int? id = null)
        {
            ShopItemModel shoppingItemModel = db.Item.Find(id);
            return shoppingItemModel;
        }
        //Get: categories/{categoryname}
        [HttpGet]
        [ActionName("Search")]
        public ActionResult Filter(String CategoryName, int? page)
        {

            IList<ShopItemModel> searchResults = new List<ShopItemModel>();
            var shoppingItem = db.Item.Include(s => s.Category).Include(s => s.Shopkeeper);
            foreach (var item in shoppingItem.OrderBy(s => s.ItemName))
            {
                if (ItemDetail(item.Reference).Category.CategoryName.ToLower() == CategoryName.ToLower())
                {
                    searchResults.Add(ItemDetail(item.Reference));
                }

            }
            return View(searchResults.ToList().ToPagedList(page ?? 1, 5));
        }
    }
}
