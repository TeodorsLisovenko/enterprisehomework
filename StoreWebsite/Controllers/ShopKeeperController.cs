﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StoreWebsite.Models;

namespace StoreWebsite.Controllers
{
    public class ShopKeeperController : Controller
    {
        private StoreDbContext db = new StoreDbContext();

        // GET: ShopKeeper
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            return View(db.User.ToList());
        }

        // GET: ShopKeeper/Details/5
        [Authorize(Roles = "Admin")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ShopKeeperModel shopKeeperModel = db.User.Find(id);
            if (shopKeeperModel == null)
            {
                return HttpNotFound();
            }
            return View(shopKeeperModel);
        }

        // GET: ShopKeeper/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: ShopKeeper/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ShopkeeperID,Name,Surname,StoreName,PhoneNumber,Email")] ShopKeeperModel shopKeeperModel)
        {
            if (ModelState.IsValid)
            {
                db.User.Add(shopKeeperModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(shopKeeperModel);
        }

        // GET: ShopKeeper/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ShopKeeperModel shopKeeperModel = db.User.Find(id);
            if (shopKeeperModel == null)
            {
                return HttpNotFound();
            }
            return View(shopKeeperModel);
        }

        // POST: ShopKeeper/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ShopkeeperID,Name,Surname,StoreName,PhoneNumber,Email")] ShopKeeperModel shopKeeperModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(shopKeeperModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(shopKeeperModel);
        }

        // GET: ShopKeeper/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ShopKeeperModel shopKeeperModel = db.User.Find(id);
            if (shopKeeperModel == null)
            {
                return HttpNotFound();
            }
            return View(shopKeeperModel);
        }

        // POST: ShopKeeper/Delete/5
        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ShopKeeperModel shopKeeperModel = db.User.Find(id);
            db.User.Remove(shopKeeperModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
