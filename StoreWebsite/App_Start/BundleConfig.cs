﻿using System.Web;
using System.Web.Optimization;

namespace StoreWebsite
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));


            //Created bundles for SLICK. Are they correct? 
          
            // The include path no doubt is correct, but bundles. Add? 

            bundles.Add(new ScriptBundle("~/bundles/slick").Include(
          "~/Scripts/Slick/slick.js"));

            bundles.Add(new StyleBundle("~/Content/slick/css").Include(
                      "~/Content/Slick/slick-theme.css",
                      "~/Content/Slick/slick.css"));

        }
    }
}
