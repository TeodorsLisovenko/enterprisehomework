﻿$(document).on('ready', function () {

    $(".carousel").slick({
        dots: false,
        infinite: true,
        centerMode: true,
        draggable: true,
        pauseOnHover: true,
        pauseOnFocus: true,
        autoplay: true,
        swipe: true,
        autoplaySpeed: 1500,
        slidesToShow: 5,
        slidesToScroll: 3,

    });

    $('.single-item').slick();

});