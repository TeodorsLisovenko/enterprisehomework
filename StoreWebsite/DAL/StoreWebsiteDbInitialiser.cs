﻿using StoreWebsite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StoreWebsite.DAL
{
    public class StoreWebsiteDbInitialiser : System.Data.Entity.DropCreateDatabaseAlways<StoreDbContext>
    {
        protected override void Seed(StoreDbContext context)
        {

            var shopKeeperList = new List<ShopKeeperModel> {
                        new ShopKeeperModel()  { ShopkeeperID = 1, Name = "Hasad", Surname = "Collier", Email = "semper.et@sagittis.ca", PhoneNumber = "848-9552", StoreName = "XXL SHOP" },
                        new ShopKeeperModel()  { ShopkeeperID = 2, Name = "Mohammad", Surname = "Chan", Email = "Integer@neque.ca", PhoneNumber = "146-2456", StoreName = "Hunk Store" },
                        new ShopKeeperModel()  { ShopkeeperID = 3, Name = "Bert", Surname = "Wilder", Email = "elit.elit.fermentum@Pellentesque.com", PhoneNumber = "1-126-851-2374", StoreName = "Hugo Market" },
                        new ShopKeeperModel()  { ShopkeeperID = 4, Name = "Xenos", Surname = "Guy", Email = "a.tortor@fermentum.org", PhoneNumber = "162-5912", StoreName = "Spot-on Store" }
             };

            shopKeeperList.ForEach(s => context.User.Add(s));
            context.SaveChanges();

            var categoryList = new List<CategoryModel> {
                    new CategoryModel() { CategoryID = 1, CategoryName = "Evening" },
                    new CategoryModel() { CategoryID = 2, CategoryName = "Sale" },
                    new CategoryModel() { CategoryID = 3, CategoryName = "Accessories" },
                    new CategoryModel() { CategoryID = 4, CategoryName = "Casual" }

             };

            categoryList.ForEach(c => context.Category.Add(c));
            context.SaveChanges();

            var ItemList = new List<ShopItemModel> {
                    new  ShopItemModel() { CategoryID = 1, ShopkeeperID = 1, ItemName = "Wedding dress", Description = "eros nec", ImageURL = "/Image/Contents/evening2.jpg", Price = 263 },
                    new  ShopItemModel() { CategoryID = 1, ShopkeeperID = 1, ItemName = "Handbag", Description = "vitae, posuere", ImageURL = "/Image/Contents/evening2.jpg", Price = 54 },
                    new  ShopItemModel() { CategoryID = 1, ShopkeeperID = 1, ItemName = "Uniform", Description = "nascetur", ImageURL = "/Image/Contents/evening2.jpg", Price = 107 },
                    new  ShopItemModel() { CategoryID = 1, ShopkeeperID = 1, ItemName = "Handbag", Description = "scelerisque sed, sapien. Nunc pulvinar", ImageURL = "/Image/Contents/evening3.jpg", Price = 124 },
                    new  ShopItemModel() { CategoryID = 1, ShopkeeperID = 1, ItemName = "Handbag", Description = "erat semper", ImageURL = "/Image/Contents/evening3.jpg", Price = 225 },
                    new  ShopItemModel() { CategoryID = 1, ShopkeeperID = 1, ItemName = "Tuxedo", Description = "nonummy ac,", ImageURL = "/Image/Contents/evening2.jpg", Price = 30 },
                    new  ShopItemModel() { CategoryID = 1, ShopkeeperID = 1, ItemName = "Uniform", Description = "Lorem ipsum dolor", ImageURL = "/Image/Contents/evening2.jpg", Price = 16 },
                    new  ShopItemModel() { CategoryID = 1, ShopkeeperID = 1, ItemName = "Uniform", Description = "pretium et, rutrum non, hendrerit", ImageURL = "/Image/Contents/evening2.jpg", Price = 65 },
                    new  ShopItemModel() { CategoryID = 1, ShopkeeperID = 1, ItemName = "Dress pants", Description = "at, egestas a, scelerisque sed,", ImageURL = "/Image/Contents/evening2.jpg", Price = 136 },
                    new  ShopItemModel() { CategoryID = 1, ShopkeeperID = 1, ItemName = "Wedding dress", Description = "Quisque imperdiet, erat nonummy ultricies ornare,", ImageURL = "/Image/Contents/evening3.jpg", Price = 214 },
                    new  ShopItemModel() { CategoryID = 1, ShopkeeperID = 1, ItemName = "Tuxedo", Description = "et arcu imperdiet", ImageURL = "/Image/Contents/evening3.jpg", Price = 97 },
                    new  ShopItemModel() { CategoryID = 1, ShopkeeperID = 1, ItemName = "Suspenders", Description = "ornare placerat, orci", ImageURL = "/Image/Contents/evening1.jpg", Price = 102 },
                    new  ShopItemModel() { CategoryID = 1, ShopkeeperID = 1, ItemName = "Wedding dress", Description = "Nullam feugiat", ImageURL = "/Image/Contents/evening2.jpg", Price = 79 },
                    new  ShopItemModel() { CategoryID = 1, ShopkeeperID = 1, ItemName = "Wedding dress", Description = "luctus felis", ImageURL = "/Image/Contents/evening1.jpg", Price = 50 },
                    new  ShopItemModel() { CategoryID = 1, ShopkeeperID = 1, ItemName = "Tuxedo", Description = "nisi. Cum sociis natoque penatibus", ImageURL = "/Image/Contents/evening3.jpg", Price = 294 },
                    new  ShopItemModel() { CategoryID = 1, ShopkeeperID = 1, ItemName = "Handbag", Description = "rhoncus. Proin nisl sem, consequat", ImageURL = "/Image/Contents/evening1.jpg", Price = 185 },
                    new  ShopItemModel() { CategoryID = 1, ShopkeeperID = 1, ItemName = "High-heeled shoes", Description = "malesuada fames ac turpis egestas. Aliquam fringilla", ImageURL = "/Image/Contents/evening1.jpg", Price = 295 },
                    new  ShopItemModel() { CategoryID = 1, ShopkeeperID = 1, ItemName = "Dress pants", Description = "ac nulla. In tincidunt congue turpis.", ImageURL = "/Image/Contents/evening1.jpg", Price = 241 },
                    new  ShopItemModel() { CategoryID = 1, ShopkeeperID = 1, ItemName = "Suspenders", Description = "dolor dapibus gravida. Aliquam tincidunt,", ImageURL = "/Image/Contents/evening1.jpg", Price = 274 },
                    new  ShopItemModel() { CategoryID = 1, ShopkeeperID = 1, ItemName = "Wedding dress", Description = "Morbi non sapien molestie orci tincidunt", ImageURL = "/Image/Contents/evening3.jpg", Price = 138 },
                    new  ShopItemModel() { CategoryID = 1, ShopkeeperID = 1, ItemName = "Dress pants", Description = "luctus aliquet odio. Etiam ligula tortor,", ImageURL = "/Image/Contents/evening1.jpg", Price = 251 },

                    new ShopItemModel() { CategoryID = 2, ShopkeeperID = 2, ItemName = "Shirts", Description = "Duis sit", ImageURL = "/Image/Contents/sale3.jpg", Price = 95 },
                    new ShopItemModel() { CategoryID = 2, ShopkeeperID = 2, ItemName = "Dress", Description = "ac tellus. Suspendisse sed", ImageURL = "/Image/Contents/sale2.jpg", Price = 38 },
                    new ShopItemModel() { CategoryID = 2, ShopkeeperID = 2, ItemName = "Shirts", Description = "at auctor", ImageURL = "/Image/Contents/sale3.jpg", Price = 113 },
                    new ShopItemModel() { CategoryID = 2, ShopkeeperID = 2, ItemName = "Pants", Description = "lorem. Donec elementum, lorem ut aliquam iaculis,", ImageURL = "/Image/Contents/sale3.jpg", Price = 88 },
                    new ShopItemModel() { CategoryID = 2, ShopkeeperID = 2, ItemName = "Pants", Description = "eu", ImageURL = "/Image/Contents/sale1.jpg", Price = 133 },
                    new ShopItemModel() { CategoryID = 2, ShopkeeperID = 2, ItemName = "Pants", Description = "rutrum magna. Cras convallis convallis dolor. Quisque", ImageURL = "/Image/Contents/sale1.jpg", Price = 6 },
                    new ShopItemModel() { CategoryID = 2, ShopkeeperID = 2, ItemName = "Dress", Description = "eget massa. Suspendisse eleifend. Cras sed", ImageURL = "/Image/Contents/sale3.jpg", Price = 131 },
                    new ShopItemModel() { CategoryID = 2, ShopkeeperID = 2, ItemName = "Pants", Description = "ornare, lectus ante dictum mi,", ImageURL = "/Image/Contents/sale1.jpg", Price = 286 },
                    new ShopItemModel() { CategoryID = 2, ShopkeeperID = 2, ItemName = "Shirts", Description = "fringilla est. Mauris eu turpis.", ImageURL = "/Image/Contents/sale1.jpg", Price = 114 },
                    new ShopItemModel() { CategoryID = 2, ShopkeeperID = 2, ItemName = "Dress", Description = "ligula consectetuer rhoncus. Nullam", ImageURL = "/Image/Contents/sale1.jpg", Price = 273 },
                    new ShopItemModel() { CategoryID = 2, ShopkeeperID = 2, ItemName = "Handbag", Description = "erat nonummy ultricies ornare, elit elit fermentum", ImageURL = "/Image/Contents/sale2.jpg", Price = 34 },
                    new ShopItemModel() { CategoryID = 2, ShopkeeperID = 2, ItemName = "Shirts", Description = "In lorem. Donec elementum, lorem ut", ImageURL = "/Image/Contents/sale2.jpg", Price = 44 },
                    new ShopItemModel() { CategoryID = 2, ShopkeeperID = 2, ItemName = "Shirts", Description = "sit", ImageURL = "/Image/Contents/sale2.jpg", Price = 143 },
                    new ShopItemModel() { CategoryID = 2, ShopkeeperID = 2, ItemName = "Dress", Description = "pede ac urna. Ut tincidunt vehicula risus.", ImageURL = "/Image/Contents/sale3.jpg", Price = 173 },
                    new ShopItemModel() { CategoryID = 2, ShopkeeperID = 2, ItemName = "Coat", Description = "malesuada fringilla est. Mauris", ImageURL = "/Image/Contents/sale3.jpg", Price = 100 },
                    new ShopItemModel() { CategoryID = 2, ShopkeeperID = 2, ItemName = "Coat", Description = "consectetuer adipiscing", ImageURL = "/Image/Contents/sale3.jpg", Price = 63 },
                    new ShopItemModel() { CategoryID = 2, ShopkeeperID = 2, ItemName = "Shirts", Description = "sit amet, consectetuer adipiscing", ImageURL = "/Image/Contents/sale1.jpg", Price = 280 },
                    new ShopItemModel() { CategoryID = 2, ShopkeeperID = 2, ItemName = "Shoes", Description = "dictum augue malesuada malesuada. Integer id", ImageURL = "/Image/Contents/sale2.jpg", Price = 154 },
                    new ShopItemModel() { CategoryID = 2, ShopkeeperID = 2, ItemName = "Coat", Description = "orci", ImageURL = "/Image/Contents/sale3.jpg", Price = 255 },
                    new ShopItemModel() { CategoryID = 2, ShopkeeperID = 2, ItemName = "Dress", Description = "sem. Nulla", ImageURL = "/Image/Contents/sale1.jpg", Price = 8 },
                    new ShopItemModel() { CategoryID = 2, ShopkeeperID = 2, ItemName = "Shirts", Description = "sollicitudin orci sem eget massa. Suspendisse eleifend.", ImageURL = "/Image/Contents/sale1.jpg", Price = 235 },

                    new ShopItemModel() { CategoryID = 3, ShopkeeperID = 3, ItemName = "Necklace", Description = "dolor elit, pellentesque a, facilisis non, bibendum", ImageURL = "/Image/Contents/accessories3.jpg", Price = 277 },
                    new ShopItemModel() { CategoryID = 3, ShopkeeperID = 3, ItemName = "Earrings", Description = "lorem", ImageURL = "/Image/Contents/accessories2.jpg", Price = 288 },
                    new ShopItemModel() { CategoryID = 3, ShopkeeperID = 3, ItemName = "Necklace", Description = "lectus pede, ultrices a,", ImageURL = "/Image/Contents/accessories3.jpg", Price = 6 },
                    new ShopItemModel() { CategoryID = 3, ShopkeeperID = 3, ItemName = "Ring", Description = "Nam", ImageURL = "/Image/Contents/accessories2.jpg", Price = 167 },
                    new ShopItemModel() { CategoryID = 3, ShopkeeperID = 3, ItemName = "Ring", Description = "eu nibh vulputate mauris sagittis placerat.", ImageURL = "/Image/Contents/accessories1.jpg", Price = 169 },
                    new ShopItemModel() { CategoryID = 3, ShopkeeperID = 3, ItemName = "Necklace", Description = "magna nec quam. Curabitur vel", ImageURL = "/Image/Contents/accessories1.jpg", Price = 95 },
                    new ShopItemModel() { CategoryID = 3, ShopkeeperID = 3, ItemName = "Ring", Description = "a, scelerisque", ImageURL = "/Image/Contents/accessories3.jpg", Price = 192 },
                    new ShopItemModel() { CategoryID = 3, ShopkeeperID = 3, ItemName = "Ring", Description = "non sapien molestie orci", ImageURL = "/Image/Contents/accessories1.jpg", Price = 45 },
                    new ShopItemModel() { CategoryID = 3, ShopkeeperID = 3, ItemName = "Necklace", Description = "sapien. Nunc pulvinar arcu et pede. Nunc", ImageURL = "/Image/Contents/accessories2.jpg", Price = 82 },
                    new ShopItemModel() { CategoryID = 3, ShopkeeperID = 3, ItemName = "Necklace", Description = "id risus quis diam luctus lobortis.", ImageURL = "/Image/Contents/accessories1.jpg", Price = 51 },
                    new ShopItemModel() { CategoryID = 3, ShopkeeperID = 3, ItemName = "Ring", Description = "tellus. Aenean egestas hendrerit neque. In", ImageURL = "/Image/Contents/accessories3.jpg", Price = 118 },
                    new ShopItemModel() { CategoryID = 3, ShopkeeperID = 3, ItemName = "Ring", Description = "vulputate, lacus. Cras interdum.", ImageURL = "/Image/Contents/accessories2.jpg", Price = 297 },
                    new ShopItemModel() { CategoryID = 3, ShopkeeperID = 3, ItemName = "Ring", Description = "tempor diam dictum sapien.", ImageURL = "/Image/Contents/accessories3.jpg", Price = 140 },
                    new ShopItemModel() { CategoryID = 3, ShopkeeperID = 3, ItemName = "Ring", Description = "rutrum lorem ac risus. Morbi", ImageURL = "/Image/Contents/accessories2.jpg", Price = 291 },
                    new ShopItemModel() { CategoryID = 3, ShopkeeperID = 3, ItemName = "Necklace", Description = "senectus", ImageURL = "/Image/Contents/accessories1.jpg", Price = 133 },
                    new ShopItemModel() { CategoryID = 3, ShopkeeperID = 3, ItemName = "Ring", Description = "Aenean massa. Integer vitae nibh. Donec est", ImageURL = "/Image/Contents/accessories1.jpg", Price = 217 },
                    new ShopItemModel() { CategoryID = 3, ShopkeeperID = 3, ItemName = "Ring", Description = "Proin mi. Aliquam gravida mauris ut", ImageURL = "/Image/Contents/accessories3.jpg", Price = 291 },
                    new ShopItemModel() { CategoryID = 3, ShopkeeperID = 3, ItemName = "Necklace", Description = "ornare placerat, orci lacus vestibulum", ImageURL = "/Image/Contents/accessories2.jpg", Price = 183 },
                    new ShopItemModel() { CategoryID = 3, ShopkeeperID = 3, ItemName = "Earrings", Description = "lectus pede, ultrices a, auctor non,", ImageURL = "/Image/Contents/accessories3.jpg", Price = 149 },
                    new ShopItemModel() { CategoryID = 3, ShopkeeperID = 3, ItemName = "Ring", Description = "enim. Etiam imperdiet dictum magna. Ut", ImageURL = "/Image/Contents/accessories2.jpg", Price = 200 },
                    new ShopItemModel() { CategoryID = 3, ShopkeeperID = 3, ItemName = "Earrings", Description = "vel, vulputate eu, odio.", ImageURL = "/Image/Contents/accessories1.jpg", Price = 40 },

                    new ShopItemModel() { CategoryID = 4, ShopkeeperID = 4, ItemName = "T-shirt", Description = "elit. Nulla facilisi.", ImageURL = "/Image/Contents/casual3.jpg", Price = 269 },
                    new ShopItemModel() { CategoryID = 4, ShopkeeperID = 4, ItemName = "Tank top", Description = "hendrerit neque. In", ImageURL = "/Image/Contents/casual3.jpg", Price = 246 },
                    new ShopItemModel() { CategoryID = 4, ShopkeeperID = 4, ItemName = "Flip-flops", Description = "mauris, rhoncus id,", ImageURL = "/Image/Contents/casual3.jpg", Price = 145 },
                    new ShopItemModel() { CategoryID = 4, ShopkeeperID = 4, ItemName = "Flip-flops", Description = "dolor sit amet, consectetuer adipiscing", ImageURL = "/Image/Contents/casual3.jpg", Price = 284 },
                    new ShopItemModel() { CategoryID = 4, ShopkeeperID = 4, ItemName = "Flip-flops", Description = "gravida. Aliquam tincidunt, nunc", ImageURL = "/Image/Contents/casual3.jpg", Price = 216 },
                    new ShopItemModel() { CategoryID = 4, ShopkeeperID = 4, ItemName = "Tank top", Description = "rutrum non, hendrerit id, ante.", ImageURL = "/Image/Contents/casual1.jpg", Price = 115 },
                    new ShopItemModel() { CategoryID = 4, ShopkeeperID = 4, ItemName = "Flip-flops", Description = "lacus. Etiam bibendum fermentum", ImageURL = "/Image/Contents/casual1.jpg", Price = 279 },
                    new ShopItemModel() { CategoryID = 4, ShopkeeperID = 4, ItemName = "Dress", Description = "enim. Etiam gravida", ImageURL = "/Image/Contents/casual1.jpg", Price = 275 },
                    new ShopItemModel() { CategoryID = 4, ShopkeeperID = 4, ItemName = "Tank top", Description = "dui nec urna suscipit nonummy. Fusce", ImageURL = "/Image/Contents/casual1.jpg", Price = 45 },
                    new ShopItemModel() { CategoryID = 4, ShopkeeperID = 4, ItemName = "Flip-flops", Description = "parturient montes, nascetur ridiculus", ImageURL = "/Image/Contents/casual3.jpg", Price = 100 },
                    new ShopItemModel() { CategoryID = 4, ShopkeeperID = 4, ItemName = "Flip-flops", Description = "bibendum ullamcorper.", ImageURL = "/Image/Contents/casual3.jpg", Price = 130 },
                    new ShopItemModel() { CategoryID = 4, ShopkeeperID = 4, ItemName = "Tank top", Description = "eget nisi", ImageURL = "/Image/Contents/casual1.jpg", Price = 113 },
                    new ShopItemModel() { CategoryID = 4, ShopkeeperID = 4, ItemName = "T-shirt", Description = "Donec tincidunt. Donec vitae erat vel pede", ImageURL = "/Image/Contents/casual2.jpg", Price = 12 },
                    new ShopItemModel() { CategoryID = 4, ShopkeeperID = 4, ItemName = "T-shirt", Description = "ipsum", ImageURL = "/Image/Contents/casual1.jpg", Price = 87 },
                    new ShopItemModel() { CategoryID = 4, ShopkeeperID = 4, ItemName = "Dress", Description = "commodo", ImageURL = "/Image/Contents/casual3.jpg", Price = 54 },
                    new ShopItemModel() { CategoryID = 4, ShopkeeperID = 4, ItemName = "Gym clothes", Description = "Sed", ImageURL = "/Image/Contents/casual2.jpg", Price = 139 },
                    new ShopItemModel() { CategoryID = 4, ShopkeeperID = 4, ItemName = "Dress", Description = "Integer urna. Vivamus molestie dapibus", ImageURL = "/Image/Contents/casual2.jpg", Price = 249 },
                    new ShopItemModel() { CategoryID = 4, ShopkeeperID = 4, ItemName = "Flip-flops", Description = "auctor.", ImageURL = "/Image/Contents/casual2.jpg", Price = 255 },
                    new ShopItemModel() { CategoryID = 4, ShopkeeperID = 4, ItemName = "Flip-flops", Description = "eleifend, nunc risus", ImageURL = "/Image/Contents/casual3.jpg", Price = 58 },
                    new ShopItemModel() { CategoryID = 4, ShopkeeperID = 4, ItemName = "T-shirt", Description = "bibendum sed, est. Nunc", ImageURL = "/Image/Contents/casual3.jpg", Price = 25 },
                    new ShopItemModel() { CategoryID = 4, ShopkeeperID = 4, ItemName = "Jacket", Description = "nascetur ridiculus mus. Proin vel", ImageURL = "/Image/Contents/casual2.jpg", Price = 268 }
            };

            ItemList.ForEach(i => context.Item.Add(i));
            context.SaveChanges();


            base.Seed(context);
        }

    }
}