﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace StoreWebsite
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {

            //TEST - First line.


            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

             routes.MapRoute(
             name: "ItemByCategorySearch",
             url: "Find-by-category/{CategoryName}",
             defaults: new { controller = "ShopItem", action = "Search", CategoryName = "Jackets" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
