﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace StoreWebsite.Models
{
    public class ShopKeeperModel
    {
        [Required]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ShopkeeperID { get; set; }

        [Required]
        [StringLength(50)]
        public String Name { get; set; }
        [Required]
        [StringLength(50)]
        public String Surname { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Store Name")]
        public String StoreName { get; set; }

        [Required]
        [Phone]
        [Display(Name = "Phone Number")]
        public String PhoneNumber { get; set; }

        [Required]
        [EmailAddress]
        public String Email { get; set; }

        [Display(Name = "Shopkeeper Items")]
        public virtual ICollection<ShopItemModel> ShoppingItems { get; set; }
    }
}