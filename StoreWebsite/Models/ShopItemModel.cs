﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace StoreWebsite.Models
{
    public class ShopItemModel
    {
        [Required]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Reference { get; set; }

        [Required]
        [Display(Name = "Item Name")]
        [StringLength(120)]
        public String ItemName { get; set; }

        [Required]
        [StringLength(120)]
        public String Description { get; set; }

        [Required]
        public String ImageURL { get; set; }

        [Range(0.0, (double)decimal.MaxValue, ErrorMessage = "The price must be 0 or more!")]
        public decimal Price { get; set; }

        [Required]
        [Display(Name = "Shopkeeper ID")]
        public int ShopkeeperID { get; set; }

        public virtual ShopKeeperModel Shopkeeper { get; set; }

        [Required]
        [Display(Name = "Category ID")]
        public int CategoryID { get; set; }

        public virtual CategoryModel Category { get; set; }

        public ShopItemModel() { }

        public ShopItemModel(int? reference = null, String itemName = "BasicItem", String description = "Empty", String imageURL = "URL", decimal price = 0.0M)
        {
            if (reference.HasValue)
            {
                this.Reference = reference.Value;
            }
            this.ItemName = itemName;
            this.Description = description;
            this.ImageURL = imageURL;
            this.Price = price;

        }
    }
}