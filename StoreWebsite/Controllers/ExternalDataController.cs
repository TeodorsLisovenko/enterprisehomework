﻿﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Upload;
using Google.Apis.Util.Store;
using Google.Apis.YouTube.v3;
using Google.Apis.YouTube.v3.Data;

namespace StoreWebsite.Controllers
{
    public class ExternalDataController : Controller
    {
        // GET: ExternalData
        [HttpPost]
        public async Task<ActionResult> Index(HttpPostedFileBase file)
        {
            var stream = file.InputStream;

            // Begin Upload
            await BeginUpload(stream);

            //  Return upload begin successful
            return new HttpStatusCodeResult(HttpStatusCode.Accepted);
        }

        private async Task BeginUpload(Stream stream, String videoTitle = "Sanijs and cruel kitchen's life", String videoDescription = "Description", string[] tags = null, string categoryId = "22", string privacyStatus = "unlisted")
        {
            // Do any validation here
            if (tags == null)
            {
                tags = new string[] { "sampleTag1", "sampleTag2" };
            }

            UserCredential credential;
            // Even though the file is added to source control, you should NOT add it to source control!
            using (var credentialStream = new FileStream(Server.MapPath("~/App_Data/client_secrets.json"), FileMode.Open, FileAccess.Read))
            {
                credential = await GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(credentialStream).Secrets,
                    // This OAuth 2.0 access scope allows an application to upload files to the
                    // authenticated user's YouTube channel, but doesn't allow other types of access.
                    new[] { YouTubeService.Scope.YoutubeUpload },
                    "user",
                    CancellationToken.None
                );
            }

            var youtubeService = new YouTubeService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = Assembly.GetExecutingAssembly().GetName().Name
            });

            var video = new Video();
            video.Snippet = new VideoSnippet();
            video.Snippet.Title = videoTitle;
            video.Snippet.Description = videoDescription;
            video.Snippet.Tags = tags;
            video.Snippet.CategoryId = categoryId; // See https://developers.google.com/youtube/v3/docs/videoCategories/list
            video.Status = new VideoStatus();
            video.Status.PrivacyStatus = privacyStatus; // or "private" or "public"

            var videosInsertRequest = youtubeService.Videos.Insert(video, "snippet,status", stream, "video/*");

            videosInsertRequest.ProgressChanged += videosInsertRequest_ProgressChanged;
            videosInsertRequest.ResponseReceived += videosInsertRequest_ResponseReceived;

            await videosInsertRequest.UploadAsync();

        }

        void videosInsertRequest_ProgressChanged(Google.Apis.Upload.IUploadProgress progress)
        {
            switch (progress.Status)
            {
                case UploadStatus.Uploading:
                    Console.WriteLine("{0} bytes sent.", progress.BytesSent);
                    break;

                case UploadStatus.Failed:
                    Console.WriteLine("An error prevented the upload from completing.\n{0}", progress.Exception);
                    break;
            }
        }

        void videosInsertRequest_ResponseReceived(Video video)
        {
            Console.WriteLine("Video id '{0}' was successfully uploaded.", video.Id);
        }
    }
}