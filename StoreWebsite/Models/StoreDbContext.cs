﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace StoreWebsite.Models
{
    public class StoreDbContext : DbContext
    {
        public StoreDbContext() : base("StoreDbContext")
        {

        }

        public DbSet<ShopKeeperModel> User { get; set; }

        public DbSet<CategoryModel> Category { get; set; }

        public DbSet<ShopItemModel> Item{ get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

    }
}